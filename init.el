;; Reusable Variables
(defvar ds/default-font-size 100)
(defvar ds/default-variable-font-size 120)
(defvar ds/fixed-font "JetBrainsMono Nerd Font")
(defvar ds/variable-font "Segoe UI")


;; Basic UI Setup
(menu-bar-mode 1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(global-tab-line-mode)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(desktop-save-mode 1)
(setq calendar-week-start-day 1)
(setq backup-inhibited t)

;; Fonts

(set-face-attribute 'default nil
                    :font ds/fixed-font
                    :height ds/default-font-size)
(set-face-attribute 'fixed-pitch nil
                    :font ds/fixed-font
                    :height ds/default-font-size)
(set-face-attribute 'variable-pitch nil
                    :font ds/variable-font
                    :height ds/default-variable-font-size
                    :weight 'regular)
(set-face-attribute 'tab-line nil
                    :font ds/variable-font
                    :height ds/default-variable-font-size
                    :weight 'regular)
(set-face-attribute 'tab-line-tab-current nil
                    :font ds/variable-font
                    :height ds/default-variable-font-size
                    :weight 'regular)

;; Styling
;; In all of the following, WEIGHT is a symbol such as `semibold',
;; `light', `bold', or anything mentioned in `modus-themes-weights'.
(setq
 modus-themes-italic-constructs t
 modus-themes-bold-constructs t
 modus-themes-mixed-fonts t
 modus-themes-variable-pitch-ui t
 modus-themes-custom-auto-reload t
 modus-themes-disable-other-themes t

 ;; Options for `modus-themes-prompts' are either nil (the
 ;; default), or a list of properties that may include any of those
 ;; symbols: `italic', `WEIGHT'
 modus-themes-prompts '(italic bold)

 ;; The `modus-themes-completions' is an alist that reads two
 ;; keys: `matches', `selection'.  Each accepts a nil value (or
 ;; empty list) or a list of properties that can include any of
 ;; the following (for WEIGHT read further below):
 ;;
 ;; `matches'   :: `underline', `italic', `WEIGHT'
 ;; `selection' :: `underline', `italic', `WEIGHT'
 modus-themes-completions '((matches . (extrabold)) (selection . (semibold italic text-also)))

 modus-themes-org-blocks 'gray-background ; {nil,'gray-background,'tinted-background}

 ;; The `modus-themes-headings' is an alist: read the manual's
 ;; node about it or its doc string.  Basically, it supports
 ;; per-level configurations for the optional use of
 ;; `variable-pitch' typography, a height value as a multiple of
 ;; the base font size (e.g. 1.5), and a `WEIGHT'.
 modus-themes-headings
 '((1 . (variable-pitch 1.5))
   (2 . (1.3))
   (agenda-date . (1.3))
   (agenda-structure . (variable-pitch light 1.8))
   (t . (1.1))))

(load-theme 'modus-vivendi-tinted t)

;; Package Manager (Elpaca)

(defvar elpaca-installer-version 0.4)
(defvar elpaca-directory
  (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory
  (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory
  (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order
  '(elpaca
    :repo "https://github.com/progfolio/elpaca.git"
    :ref nil
    :files (:defaults (:exclude "extensions"))
    :build (:not elpaca--activate-package)))
(let* ((repo (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list
   'load-path
   (if (file-exists-p build)
       build
     repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28)
      (require 'subr-x))
    (condition-case-unless-debug err
        (if-let ((buffer
                  (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                 ((zerop
                   (call-process "git"
                                 nil
                                 buffer
                                 t
                                 "clone"
                                 (plist-get order :repo)
                                 repo)))
                 ((zerop
                   (call-process "git"
                                 nil
                                 buffer
                                 t
                                 "checkout"
                                 (or (plist-get order :ref) "--"))))
                 (emacs (concat invocation-directory invocation-name))
                 ((zerop
                   (call-process
                    emacs
                    nil
                    buffer
                    nil
                    "-Q"
                    "-L"
                    "."
                    "--batch"
                    "--eval"
                    "(byte-recompile-directory \".\" 0 'force)")))
                 ((require 'elpaca))
                 ((elpaca-generate-autoloads "elpaca" repo)))
          (kill-buffer buffer)
          (error
           "%s"
           (with-current-buffer buffer
             (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

;; Install use-package support
(elpaca
 elpaca-use-package
 ;; Enable :elpaca use-package keyword.
 (elpaca-use-package-mode)
 ;; Assume :elpaca t unless otherwise specified.
 (setq elpaca-use-package-by-default t))

;; Block until current queue processed.
(elpaca-wait)

(use-package rainbow-delimiters
  :config (rainbow-delimiters-mode 1))

(use-package all-the-icons)

(use-package evil)

(use-package treemacs-all-the-icons
  :after (treemacs all-the-icons)
  :config (treemacs-load-theme "all-the-icons"))

(use-package
 treemacs
 :config (progn
	   (setq
	    treemacs-collapse-dirs (if treemacs-python-executable 3 0))
	   (treemacs-follow-mode t)
	   (treemacs-filewatch-mode t)
	   (treemacs-fringe-indicator-mode 'always)
	   (treemacs-git-commit-diff-mode t))
	 (treemacs-git-mode 'deferred)
 :custom-face
 (treemacs-directory-face ((t (:family Inter :weight semibold))))
 (treemacs-file-face ((t (:inherit treemacs-directory-face))))
 (treemacs-root-face ((t (:inherit treemacs-directory-face :underline nil))))
 (treemacs-git-ignored-face ((t (:inherit treemacs-directory-face :foreground "darkgrey"))))
 (treemacs-git-untracked-face ((t (:inherit treemacs-directory-face))))
 (treemacs-git-modified-face ((t (:inherit treemacs-directory-face :weight bold :foreground "pink"))))
 )


(setq treemacs-indentation 2)

(use-package
 treemacs-evil
 :requires (evil treemacs)
 :after (treemacs)
 :config (evil-mode))

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

;; (treemacs-modify-theme "all-the-icons"
;;   :config
;;   (progn
;;     (treemacs-create-icon :icon "+" :extensions (dir-closed))
;;     (treemacs-create-icon :icon "-" :extensions (dir-open))))

(use-package magit)

(use-package projectile)

(use-package
 evil-leader
 :requires (evil)
 :after (evil)
 :config
 (global-evil-leader-mode)
 (evil-leader/set-leader "<SPC>")
 (evil-leader/set-key
   "e" 'treemacs
   "ti" 'treemacs-hide-gitignored-file-mode
   "ff" 'find-file
   "k" 'kill-buffer))
